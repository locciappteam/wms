﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Json
{
    public class jsonRetornoFaturamento
    {
        public int Codigo { get; set; }
        public string Protocolo { get; set; }
        public string NUMERO_ORDEM_EXTERNA_ID { get; set; }
        public object SOH_SUSR3 { get; set; }
        public string SOH_C_COMPANY { get; set; }
        public string SOH_C_ADDRESS1 { get; set; }
        public string SOH_C_ADDRESS2 { get; set; }
        public string SOH_C_ADDRESS3 { get; set; }
        public string SOH_C_CITY { get; set; }
        public string SOH_C_STATE { get; set; }
        public string SOH_C_ZIP { get; set; }
        public string SOH_C_COUNTRY { get; set; }
        public string SOH_B_COMPANY { get; set; }
        public string SOH_B_ADDRESS1 { get; set; }
        public string SOH_B_ADDRESS2 { get; set; }
        public string SOH_B_ADDRESS3 { get; set; }
        public string SOH_B_CITY { get; set; }
        public string SOH_B_STATE { get; set; }
        public string SOH_B_ZIP { get; set; }
        public string SOH_B_COUNTRY { get; set; }
        public string SOH_QTD_VOL { get; set; }
        public string CX_QT_TOT_CX { get; set; }
        public string CX_PS_BRU_TOT { get; set; }
        public string CX_PS_LIQ_TOT { get; set; }
    }
}
