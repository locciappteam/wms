﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Json
{
    public class jsonResponseNFSenior
    {
        public bool sucesso { get; set; }
        public string mensagem { get; set; }
        public string protocolo { get; set; }
    }
}
