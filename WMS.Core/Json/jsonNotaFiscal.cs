﻿using System;

namespace WMS.Core.Json
{
    public class jsonNotaFiscal
    {
        public string CNPJ { get; set; }
        public int Status { get; set; }
        public string CodigoExterno { get; set; }
        public string NotaFiscalPDF { get; set; }

        //[JsonIgnore]
        public string NotaFiscalXML { get; set; }

        /*
        [JsonProperty("NotaFiscalXML")]
        public string NotaFiscalXML_Base64
        {
            get
            {
                return NotaFiscalXML = Convert.ToBase64String(File.ReadAllBytes(NotaFiscalXML));
            }
            set
            {
                Convert.ToBase64String(File.ReadAllBytes(NotaFiscalXML));
            }
        }
        */

        public string Protocolo { get; set; }

        public string NumNota { get; set; }
        public string SerieNota { get; set; }
        public string ChaveNota { get; set; }
        public DateTime DataEmissaoNota { get; set; }
        public int Peso { get; set; }
        public int? Volume { get; set; }
        public string CNPJTransportadora { get; set; } 
    }
}
