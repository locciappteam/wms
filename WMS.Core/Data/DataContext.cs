﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WMS.Core.Entidades;
using WMS.Core.Entidades.Views;

namespace WMS.Core.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        {
           // this.Database.CommandTimeout = 300;
        }

        public DbSet<W_WMS_NOTA_FATURADA> wWmsNotaFaturada { get; set; }
      
        public DbSet<WMS_PARAMETROS> WmsParametros { get; set; }
        public DbSet<WMS_PEDIDO_CAPA> WmsPedidoCapa { get; set; }
        public DbSet<WMS_PEDIDO_ENDERECO> WmsPedidoEndereco { get; set; }

        public DbSet<WMS_ENVIO_XML_FTP_SENIOR> WmsEnvioXmlFTPSenior { get; set; }
        public DbSet<WMS_NFE_CONTROLE> WmsNfeControle { get; set; }
        public DbSet<WMS_NFE_RETORNO> WmsNfeRetorno { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
