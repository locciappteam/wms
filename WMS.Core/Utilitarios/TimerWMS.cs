﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using WMS.Core.API;
using WMS.Core.Data;

namespace WMS.Core.Utilitarios
{
    public class TimerWMS
    {
        static Timer timer;

        public static void TimeScheduler()
        {
            try
            {
                Log.Write(strMensagem: String.Format($"{DateTime.Now} : Realizando agendamento..."), Tipo: 0);

                DateTime ScheduledTime = DateTime.Now.AddMinutes(10);

                Log.Write(String.Format($"{DateTime.Now} : Tarefa agendada para {ScheduledTime}"));

                double TickTime = (double)(ScheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new Timer(TickTime);
                timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : Erro Fatal: {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "dlbr-itapplications@loccitane.com", ex.ToString());
            }
        }

        private static void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));

            try
            {
                string User;                                                                         
                string Pass;
                Uri baseAddress;

                string ftpHost;
                string ftpPort;
                string ftpUser;
                string ftpPassword;
                string ftpPath;

                using (DataContext _ctx = new DataContext())
                {
                    User = _ctx.WmsParametros.Find("USUARIO_API").VALOR;
                    Pass = _ctx.WmsParametros.Find("SENHA_API").VALOR;
                    baseAddress = new Uri(_ctx.WmsParametros.Find("BASE_ADDRESS").VALOR);

                    ftpHost = _ctx.WmsParametros.Find("SENIOR_FTP_HOST_PROD").VALOR;
                    ftpPort = _ctx.WmsParametros.Find("SENIOR_FTP_PORT_PROD").VALOR;
                    ftpUser = _ctx.WmsParametros.Find("SENIOR_FTP_USER_PROD").VALOR;
                    ftpPassword = _ctx.WmsParametros.Find("SENIOR_FTP_PASSWORD_PROD").VALOR;
                    ftpPath = _ctx.WmsParametros.Find("SENIOR_FTP_FOLDER_INTEG_PROD").VALOR;
                }

                byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
                string Auth = Convert.ToBase64String(byteAuth);

                ImportaNotas.Cegid(DateTime.Today.AddDays(-5), new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1));
                ApiExportaNotasSenior.toFTP(ftpHost, ftpPort, ftpUser, ftpPassword, ftpPath);

                Task.Run(async () =>
                {
                   ///await apiNotaFaturada.Post(Auth, baseAddress);
                   await apiDadosFaturamento.Get(Auth, baseAddress);
                   await apiConfirmaDownload.Post(Auth, baseAddress);

                }).Wait();


            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "wagner.damasio@loccitane.com", ex.ToString());
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
            TimeScheduler();
        }
    }
}
