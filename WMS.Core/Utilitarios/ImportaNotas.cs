﻿using System;
using System.Data;
using System.Data.SqlClient;
using WMS.Core.Data;


namespace WMS.Core.Utilitarios
{
    public class ImportaNotas
    {
        public static void Cegid(DateTime DataInicial, DateTime DataFinal)
        {
            Log.Write(String.Format($"{DateTime.Now} : Cegid - Importando notas faturadas"));

            using(  DataContext _ctx = new DataContext())
            {
                _ctx.Database.CommandTimeout = 300;

                try
                {

                    SqlParameter in1 = new SqlParameter
                    {
                        ParameterName = "DTINICIAL",
                        SqlDbType = SqlDbType.DateTime,
                        Value = DataInicial
                        
                    };

                    SqlParameter in2 = new SqlParameter
                    {
                        ParameterName = "DTFINAL",
                        SqlDbType = SqlDbType.DateTime,
                        Value = DataFinal
                    };

                    SqlParameter out1 = new SqlParameter
                    {
                        ParameterName = "RESULT_NFE_CONTROLE",
                        Size = int.MaxValue,
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output,
                    };

                    SqlParameter out2 = new SqlParameter
                    {
                        ParameterName = "RESULT_FTP_SENIOR",
                        Size = int.MaxValue,
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output,
                    };


                    _ctx.Database.ExecuteSqlCommand("EXEC dbo.usp_ATUALIZA_WMS_NOTA_FATURADA_NEW @DTINICIAL, @DTFINAL, @RESULT_NFE_CONTROLE OUT, @RESULT_FTP_SENIOR OUT", in1,in2, out1, out2);

                    var qtdeNotasControl = out1.Value;
                    var qtdeNotasSenior = out2.Value;

                    Log.Write(String.Format($"{DateTime.Now} : " + qtdeNotasControl.ToString() + " Notas Importadas"));
                    Log.Write(String.Format($"{DateTime.Now} : " + qtdeNotasSenior.ToString() + " Notas Enviadas para Fila Sênior"));

                }
                catch (Exception ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : Erro ao importar notas: " + ex.Message));
                }
 
            }
        }

        public static void procAtualizaFilaSenior(string chaveNFE, string successMessage)
        {
            Log.Write(String.Format($"{DateTime.Now} : Cegid - Executando Procedure Atualiza FIlaSenior"));

            using (DataContext _ctx = new DataContext())
            {
                _ctx.Database.CommandTimeout = 300;

                try
                {

                    SqlParameter in1 = new SqlParameter
                    {
                        ParameterName = "CHAVE_NFE",
                        SqlDbType = SqlDbType.VarChar,
                        Value = chaveNFE

                    };

                    SqlParameter in2 = new SqlParameter
                    {
                        ParameterName = "SUCCESS_MESSAGE",
                        SqlDbType = SqlDbType.VarChar,
                        Value = successMessage
                    };

                    SqlParameter out1 = new SqlParameter
                    {
                        ParameterName = "RETORNO",
                        SqlDbType = SqlDbType.Bit,
                        Direction = ParameterDirection.Output
                    };

                    var tst = _ctx.Database.ExecuteSqlCommand("EXEC dbo.usp_ATUALIZA_WMS_ENVIO_XML_FTP_SENIOR_POR_CHAVENFE @CHAVE_NFE, @SUCCESS_MESSAGE, @RETORNO OUTPUT", in1, in2, out1);

                    if((bool)out1.Value)
                        Log.Write(String.Format($"{DateTime.Now} : " + chaveNFE + " Notas Importadas (ALTERNATIVO)"));
                    else
                    {
                        throw new Exception($"CHAVE NFE {chaveNFE} não encontrada");
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : Erro ao importar notas (ALTERNATIVO): " + ex.Message));
                }

            }
        }


    }

}
