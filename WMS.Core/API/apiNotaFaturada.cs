﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WMS.Core.Data;
using WMS.Core.Entidades;
using WMS.Core.Json;
using WMS.Core.Utilitarios;

namespace WMS.Core.API
{
    public class apiNotaFaturada
    {
        public static async Task Post(string Auth, Uri baseAddress)
         {
            Log.Write(String.Format($"{DateTime.Now} : API - Nota Faturada"));

            List<jsonNotaFiscal> _jsonNotaFiscal = new List<jsonNotaFiscal>();
            using (DataContext _ctx = new DataContext())
            {
                _jsonNotaFiscal = (
                    from
                        a in _ctx.WmsNfeControle
                    from
                        b in _ctx.WmsNfeRetorno.Where(retorno => retorno.CHAVE_NFE == a.CHAVE_NFE).DefaultIfEmpty()

                    from c in _ctx.WmsEnvioXmlFTPSenior.Where(p=> p.CHAVE_NFE == a.CHAVE_NFE).DefaultIfEmpty()
                    where
                        b.CHAVE_NFE == null & c.ENVIADO == false
                    //b.MENSAGEM  == "TESTE"
                    select new jsonNotaFiscal
                    {
                        CNPJ = a.ESTABELECIMENTO.CNPJ,
                        Status = 100,
                        CodigoExterno = a.REF_INTERNA,
                        NotaFiscalPDF = "",
                        NotaFiscalXML = "",
                        Protocolo = a.PROTOCOLO,
                        NumNota = a.NUMERO_NF.ToString(),
                        SerieNota = a.SERIE_NF,
                        ChaveNota = a.CHAVE_NFE,
                        DataEmissaoNota = a.DATA_EMISSAO,
                        Peso = a.PESO,
                        Volume = a.VOLUME == null ? 1 : a.VOLUME,
                        CNPJTransportadora = a.CNPJ_TRANSPORTADORA

                    }
                ).ToList();
            }


            for(int i = 0; i < _jsonNotaFiscal.Count(); i += 10)
            {
                string NotaFaturadaJson = JsonConvert.SerializeObject(_jsonNotaFiscal.Skip(i).Take(10));

                Log.Write(String.Format($"{DateTime.Now} : Enviando Notas "));

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    httpClient.Timeout = TimeSpan.FromMinutes(10);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Auth);

                    using (var content = new StringContent(NotaFaturadaJson, Encoding.Default, "application/json"))
                    {
                        using (var response = await httpClient.PostAsync("NotaFiscal", content))
                        {
                            string responseData = await response.Content.ReadAsStringAsync();
                            List<jsonResponseNotaFiscal> _jsonResponseNotaFiscal = JsonConvert.DeserializeObject<List<jsonResponseNotaFiscal>>(responseData);

                            if (response.IsSuccessStatusCode)
                            {
                                try
                                {
                                    List<jsonNotaFiscal> NotasEnviadas = _jsonNotaFiscal.Skip(i).Take(10).ToList();
                                    foreach (var Nota in NotasEnviadas)
                                    { 
                                        WMS_NFE_RETORNO WmsNfeRetorno = new WMS_NFE_RETORNO
                                        {
                                            CHAVE_NFE = Nota.ChaveNota,
                                            PROTOCOLO = _jsonResponseNotaFiscal[NotasEnviadas.IndexOf(Nota)].Protocolo,
                                            MENSAGEM = _jsonResponseNotaFiscal[NotasEnviadas.IndexOf(Nota)].Mensagem,
                                            CADASTRADO_POR = "INTEGRACAO",
                                            DATA_CADASTRO = DateTime.Now,
                                            ALTERADO_POR = "INTEGRACAO",
                                            DATA_ALTERACAO = DateTime.Now
                                        };

                                        using (DataContext _ctx = new DataContext())
                                        {
                                            _ctx.WmsNfeRetorno.Add(WmsNfeRetorno);
                                            _ctx.SaveChanges();
                                        }

                                        ImportaNotas.procAtualizaFilaSenior(WmsNfeRetorno.CHAVE_NFE, "Enviado para EXVIEW");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Write(String.Format($"{DateTime.Now} :: Nota Faturada - Erro ao Salvar retorno \n { ex.Message}"));
                                    throw;
                                }
                            }
                            else
                            {
                                Log.Write(String.Format($"{DateTime.Now} :: Nota Faturada - Erro na chamada \n {response.StatusCode} - {response.RequestMessage}"));
                            }
                        }
                    }
                }
            }
            
        }
    }
}
