﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WMS.Core.Data;
using WMS.Core.Entidades;
using WMS.Core.Json;
using WMS.Core.Utilitarios;

namespace WMS.Core.API
{
    public class apiConfirmaDownload
    {
        public static async Task Post(string Auth, Uri baseAddress)
        {
            Log.Write(String.Format($"{DateTime.Now} : API - Confirma Download"));

            List<jsonConfirmaDownload> _jsonConfirmaDownload;
            using (DataContext _ctx = new DataContext())
            {
                _jsonConfirmaDownload = (
                    from
                        a in _ctx.WmsPedidoCapa
                    where
                        a.DOWNLOAD == null || a.DOWNLOAD == false
                    orderby a.DATA_CADASTRO descending
                    select new jsonConfirmaDownload
                    {
                        Codigo = 1,
                        //Mensagem = "Download realizado com sucesso!",
                        Protocolo = a.PROTOCOLO
                    }
                ).ToList();
            }

            for(int i = 0; i < _jsonConfirmaDownload.Count(); i += 10)
            {
                Log.Write(String.Format($"{DateTime.Now} : Confirmando Download - {i}/{_jsonConfirmaDownload.Count}"));

                string ConfirmaDownloadJson = JsonConvert.SerializeObject(_jsonConfirmaDownload.Skip(i).Take(10));

                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Auth);
                    httpClient.Timeout = TimeSpan.FromMinutes(20);

                    using (var content = new StringContent(ConfirmaDownloadJson, Encoding.Default, "application/json"))
                    {
                        using (var response = await httpClient.PostAsync("Confirmar", content))
                        {
                            string responseData = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                try
                                {
                                    foreach (var ConfirmaDownload in _jsonConfirmaDownload.Skip(i).Take(10))
                                    {
                                        WMS_PEDIDO_CAPA WmsPedidoCapa;
                                        using (DataContext _ctx = new DataContext())
                                        {
                                            WmsPedidoCapa = _ctx.WmsPedidoCapa.Where(a => a.PROTOCOLO == ConfirmaDownload.Protocolo).First();
                                        }
                                        using (DataContext _ctx = new DataContext())
                                        {
                                            WmsPedidoCapa.DOWNLOAD = true;

                                            _ctx.Entry(WmsPedidoCapa).State = EntityState.Modified;
                                                         
                                                _ctx.SaveChanges();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Write(String.Format($"{DateTime.Now} :: Confirma Download - Erro ao Salvar pedido { ex.Message }"));
                                    continue;
                                }
                            }
                            else
                            {
                                Log.Write(String.Format($"{DateTime.Now} :: Confirma Download - Erro na chamada \n {response.StatusCode} - {response.RequestMessage}"));
                            }
                        }
                    }
                }
            }
        }
    }
}
