﻿using System;
using System.Collections.Generic;
using System.Linq;
using WMS.Core.Data;
using WMS.Core.Entidades;
using WMS.Core.Utilitarios;
using FluentFTP;
using System.IO;
using System.Xml.Linq;

namespace WMS.Core.API
{
    public class ApiExportaNotasSenior
    {
       
       public static void toFTP(string host, string port, string userName, string password, string ftpPath )
        {
            Log.Write(string.Format($"{DateTime.Now} : FTP - Exportação de Notas Faturadas para Sênior Iniciada"));
            List<string> listaCaminhoSalvo = new List<string>();
            try
            {
                var files = getFiles();
                Log.Write(string.Format($"{DateTime.Now} : FTP - Encontrado(s) {files.Count} arquivo(s) para exportação"));

                if (files.Count > 0)
                {
                    using (var ftp = new FtpClient(host, Convert.ToInt32(port), userName, password))
                    {
                        ftp.Connect();
                        ftp.SetWorkingDirectory(ftpPath);
                        if (ftp.IsConnected)
                        {
                            int cont = 0;
                            foreach (WMS_ENVIO_XML_FTP_SENIOR file in files)
                            {
                                cont++;
                                if (File.Exists(file.ARQUIVO_XML))
                                {
                                    var caminhoSalvo = TransformaXML(file.ARQUIVO_XML);
                                    listaCaminhoSalvo.Add(caminhoSalvo);

                                    FileInfo info = null; 

                                    if (File.Exists(caminhoSalvo)) { 
                                        info = new FileInfo(caminhoSalvo);
                                    }
                                    else 
                                    { 
                                        throw new Exception("Arquivo editado não salvo");
                                    }
                                 
                                    var status = ftp.UploadFile(info.FullName, info.Name, FtpRemoteExists.Overwrite,false,FtpVerify.Delete);

                                    if (status.IsSuccess())
                                    {
                                        file.RETORNO_SUCESSO = "Sucesso";
                                        file.ENVIADO = true;
                                        file.RETORNO_FALHO = null;
                                        file.DESC_ERRO = null;

                                        Log.Write(string.Format($"{DateTime.Now} : FTP - {cont}/{files.Count}  Arquivo {file.ARQUIVO_XML} exportado com Sucesso"));
                                    }
                                    else
                                    {
                                        file.RETORNO_FALHO = "Falha";
                                        file.ENVIADO = false;
                                        file.DESC_ERRO = "Erro ao enviar arquivo";
                                        Log.Write(string.Format($"{DateTime.Now} : FTP - {cont}/{files.Count} Arquivo {file.ARQUIVO_XML} não exportado"));
                                    }

                                    using (var _ctx = new DataContext())
                                    {
                                        _ctx.Entry(file).State = System.Data.Entity.EntityState.Modified;
                                        _ctx.SaveChanges();
                                    }
                                }
                                else
                                {
                                    Log.Write(string.Format($"{DateTime.Now} : FTP - {cont}/{files.Count}  Arquivo {file.ARQUIVO_XML} não encontrado"));
                                }

                            }
                        }
                    }                   
                }
                Log.Write(string.Format($"{DateTime.Now} : FTP - Exportação de Notas Faturadas para Sênior Finalizada"));
            }
            catch (Exception ex)
            {
                Log.Write(string.Format($"{ DateTime.Now} : ERRO FATAL ao exportar Notas Faturadas via FTP para Sênior : {ex.Message}"));
                
            }
            finally
            {
                try
                {
                    foreach (var item in listaCaminhoSalvo)
                    {
                        File.Delete(item);
                    }
                }
                catch
                {

                }
                            
            }
        }

        public static List<WMS_ENVIO_XML_FTP_SENIOR> getFiles()
        {
            List<WMS_ENVIO_XML_FTP_SENIOR> lista; 
            using (var _ctx = new DataContext())
            {
                DateTime DataInicial = DateTime.Now.AddMonths(-3);

                lista = _ctx.Set<WMS_ENVIO_XML_FTP_SENIOR>().Where(p => p.ENVIADO == false).ToList();
                //var listRet = _ctx.Set<WMS_NFE_RETORNO>().Where(p => p.DATA_CADASTRO >= DataInicial).ToList();

                /*
                var t = (
                        from a in listXML
                        join b in listRet on a.CHAVE_NFE equals b.CHAVE_NFE into _res
                        from c in _res.DefaultIfEmpty()
                        select c
                        ).Distinct().Where(p=> p.CHAVE_NFE != "").ToList();
                */
            }
            return lista;
        }

        public static string TransformaXML(string ArquivoOrigem) 
        {
            try
            {
                string caminhoSalvo = "";

                if (File.Exists(ArquivoOrigem))
                {
                    FileInfo fileInfo = new FileInfo(ArquivoOrigem);
                    string xml = File.ReadAllText(fileInfo.FullName);

                    XDocument xDoc = XDocument.Parse(xml);
                    XNamespace nameSpace = "http://www.portalfiscal.inf.br/nfe";

                    // ALTERA O VALOR DO ELEMENTO IE PARA EM BRANCO 
                   
                    /* ENVIO DA INSCRIÇÃO ESTADUAL DESABILITADO
                     * 
                     * 
                    var IE = from a in xDoc.Descendants(nameSpace + "emit") select a.Element(nameSpace + "IE");
                    foreach (var item in IE)
                    {
                        item.Value = "";
                    }
                    */

                    // ALTERA O VALOR DO ELEMENTO XPED RETIRANDO O PREFIXO (BR0001-) QUANDO TIVER
                    var result = from a in xDoc.Descendants(nameSpace + "det").Elements(nameSpace + "prod") select new { prod = a.Element(nameSpace + "xPed") };
                    foreach (var item in result)
                    {
                        var newer = item.prod.Value.Replace("BR0001-", "");
                        item.prod.Value = newer;
                    }

                    caminhoSalvo = Environment.CurrentDirectory + "\\" + fileInfo.Name;
                    xDoc.Save(Environment.CurrentDirectory + "\\" + fileInfo.Name);
                }

                return caminhoSalvo;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

    }
}
 