﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WMS.Core.Data;
using WMS.Core.Entidades;
using WMS.Core.Json;
using WMS.Core.Utilitarios;

namespace WMS.Core.API
{
    public class apiDadosFaturamento
    {
        public static async Task Get(string Auth, Uri baseAddress)
        {
            Log.Write(String.Format($"{DateTime.Now} : API - Dados Faturamento"));

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Auth);
                httpClient.Timeout = TimeSpan.FromMinutes(1);

                using (var response = await httpClient.GetAsync("RetornoFaturamento"))
                {
                    string responseData = await response.Content.ReadAsStringAsync();
                    
                    if(response.IsSuccessStatusCode)
                    {
                        List<jsonRetornoFaturamento> _jsonRetornoFaturamento = JsonConvert.DeserializeObject<List<jsonRetornoFaturamento>>(responseData);
                        List<WMS_PEDIDO_CAPA> WmsPedidoCapa = (
                            from
                                a in _jsonRetornoFaturamento
                            select new WMS_PEDIDO_CAPA
                            {
                                REF_INTERNA = a.NUMERO_ORDEM_EXTERNA_ID,
                                CODIGO = a.Codigo,
                                PROTOCOLO = a.Protocolo,
                                QTDE_VOLUME = Convert.ToInt16(a.SOH_QTD_VOL),
                                QTDE_CAIXA = Convert.ToInt16(a.CX_QT_TOT_CX),
                                PESO_BRUTO = a.CX_PS_BRU_TOT,
                                PESO_LIQUIDO = a.CX_PS_LIQ_TOT,
                                NOTA_PRESENTE = Convert.ToString(a.SOH_SUSR3),
                                DOWNLOAD = false,
                                CADASTRADO_POR = "INTEGRACAO",
                                DATA_CADASTRO = DateTime.Now,
                                ALTERADO_POR = "INTEGRACAO",
                                DATA_ALTERACAO = DateTime.Now,
                                WMS_PEDIDO_ENDERECO = new WMS_PEDIDO_ENDERECO
                                {
                                    REF_INTERNA = a.NUMERO_ORDEM_EXTERNA_ID,
                                    FAT_CLIENTE = a.SOH_B_COMPANY,
                                    FAT_ENDERECO = a.SOH_B_ADDRESS1,
                                    FAT_COMPLEMENTO = a.SOH_B_ADDRESS2,
                                    FAT_BAIRRO = a.SOH_B_ADDRESS3,
                                    FAT_CIDADE = a.SOH_B_CITY,
                                    FAT_UF = a.SOH_B_STATE,
                                    FAT_CEP = a.SOH_B_ZIP,
                                    FAT_PAIS = a.SOH_B_COUNTRY,
                                    DEST_CLIENTE = a.SOH_C_COMPANY,
                                    DEST_ENDERECO = a.SOH_C_ADDRESS1,
                                    DEST_COMPLEMENTO = a.SOH_C_ADDRESS2,
                                    DEST_BAIRRO = a.SOH_C_ADDRESS3,
                                    DEST_CIDADE = a.SOH_C_CITY,
                                    DEST_UF = a.SOH_C_STATE,
                                    DEST_CEP = a.SOH_C_ZIP,
                                    DEST_PAIS = a.SOH_C_COUNTRY,
                                    CADASTRADO_POR = "INTEGRACAO",
                                    DATA_CADASTRO = DateTime.Now,
                                    ALTERADO_POR = "INTEGRACAO",
                                    DATA_ALTERACAO = DateTime.Now
                                }
                            }
                        ).ToList();

                        
                        foreach (var Pedido in WmsPedidoCapa)
                        {
                            try
                            {
                                Pedido.DOWNLOAD = false;

                                WMS_PEDIDO_CAPA _WmsPedidoCapa;
                                using(DataContext _ctx = new DataContext())
                                {
                                    _WmsPedidoCapa = _ctx.WmsPedidoCapa.Find(Pedido.REF_INTERNA);
                                }
                                using (DataContext _ctx = new DataContext())
                                {
                                    if(_WmsPedidoCapa == null)
                                    {
                                        _ctx.Set<WMS_PEDIDO_CAPA>().Add(Pedido);
                                        _ctx.SaveChanges();
                                    }
                                    else
                                    {
                                        _ctx.Entry(Pedido).State = EntityState.Modified;
                                        _ctx.SaveChanges();

                                        _ctx.Entry(Pedido.WMS_PEDIDO_ENDERECO).State = EntityState.Modified;
                                        _ctx.SaveChanges();
                                    } 
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Write(String.Format($"{DateTime.Now} :: Dados Faturamento - Erro ao Salvar pedido {Pedido.REF_INTERNA} \n { ex.Message}"));
                                continue;
                            }
                        }                        
                    }
                    else
                    {
                        Log.Write(String.Format($"{DateTime.Now} :: Dados Faturamento - Erro na chamada \n {response.StatusCode} - {response.RequestMessage}"));
                    }
                }
            }
        }
    }
}