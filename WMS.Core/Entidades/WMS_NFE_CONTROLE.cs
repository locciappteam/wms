﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Entidades
{
    public class WMS_NFE_CONTROLE
    {
        public WMS_NFE_CONTROLE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [ForeignKey("WMS_NFE_RETORNO")]
        public string CHAVE_NFE { get; set; }
        public string REF_INTERNA { get; set; }

        [ForeignKey("ESTABELECIMENTO")]
        public int ESTABELECIMENTOID { get; set; }
        public int NUMERO_NF { get; set; }
        public string SERIE_NF { get; set; }
        public string STATUS_NF { get; set; }
        public string PROTOCOLO { get; set; }
        public DateTime DATA_EMISSAO { get; set; }
        public int PESO { get; set; }
        public int? VOLUME { get; set; }

        public string CNPJ_TRANSPORTADORA { get; set; }
        public string XML_NOTA { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual WMS_NFE_RETORNO WMS_NFE_RETORNO { get; set; }
        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }
    }
}
