﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WMS.Core.Entidades.Views
{
    [Table("W_WMS_NOTA_FATURADA_NEW")]
    public class W_WMS_NOTA_FATURADA
    {
        [Key]
        public string CHAVE_NFE { get; set; }
        public string CNPJ { get; set; }
        public int ESTABELECIMENTOID { get; set; }
        public string REF_INTERNA { get; set; }
        public DateTime DATA_EMISSAO { get; set; }
        public int NUMERO_NF { get; set; }
        public string SERIE_NF { get; set; }
        public string STATUS_NF { get; set; }
        public string PROTOCOLO { get; set; }
        public string VOLUME { get; set; }
        public int PESO { get; set; }
        public string ARQUIVO_XML { get; set; }
        public DateTime DATA_AUTORIZACAO { get; set; }
        public string CNPJ_TRANSPORTADORA { get; set; }
    }
}
