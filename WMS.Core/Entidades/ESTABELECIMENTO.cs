﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Entidades
{
    public class ESTABELECIMENTO
    {
        public int ID { get; set; }
        public string COD_ESTAB { get; set; }
        public string DESC_ESTAB { get; set; }
        public string CNPJ { get; set; }
        public string IE { get; set; }
        public string CEP { get; set; }
        public string ENDERECO { get; set; }
        public string BAIRRO { get; set; }
        public string COMPLEMENTO { get; set; }
        public string CIDADE { get; set; }
        public int? ESTADOID { get; set; }
        public string EMAIL { get; set; }
        public string CENTRO_CUSTO { get; set; }
        public int? REGIONALID { get; set; }
        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<WMS_NFE_CONTROLE> WMS_NFE_CONTROLEs { get; set; }
    }
}
