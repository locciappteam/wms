﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WMS.Core.Entidades
{
    public class WMS_ENVIO_XML_FTP_SENIOR
    {

        public WMS_ENVIO_XML_FTP_SENIOR()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string CHAVE_NFE { get; set; }      
        public string ARQUIVO_XML { get; set; }
        public bool ENVIADO { get; set; }
 
        public string RETORNO_SUCESSO {get;set;}
        public string RETORNO_FALHO { get; set; }
        public string DESC_ERRO { get; set; }
        
        public string CADASTRADO_POR { get; set; }
        public DateTime? DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime? DATA_ALTERACAO { get; set; }       
    }
}
