﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Entidades
{
    public class WMS_NFE_RETORNO
    {
        [Key]
        [ForeignKey("WMS_NFE_CONTROLE")]
        public string CHAVE_NFE { get; set; }

        public string PROTOCOLO { get; set; }
        public string MENSAGEM { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual WMS_NFE_CONTROLE WMS_NFE_CONTROLE { get; set; }
    }
}
