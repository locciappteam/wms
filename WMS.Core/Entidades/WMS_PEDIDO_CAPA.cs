﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Entidades
{
    public class WMS_PEDIDO_CAPA
    {
        public WMS_PEDIDO_CAPA()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [ForeignKey("WMS_PEDIDO_ENDERECO")]
        public string REF_INTERNA { get; set; }

        public int? CODIGO { get; set; }
        public string PROTOCOLO { get; set; }
        public int? QTDE_VOLUME { get; set; }
        public int? QTDE_CAIXA { get; set; }
        public string PESO_BRUTO { get; set; }
        public string PESO_LIQUIDO { get; set; }
        public string NOTA_PRESENTE { get; set; }
        public bool? DOWNLOAD { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual WMS_PEDIDO_ENDERECO WMS_PEDIDO_ENDERECO { get; set; }
    }
}
