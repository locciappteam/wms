﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Core.Entidades
{
    public class WMS_PEDIDO_ENDERECO
    {
        public WMS_PEDIDO_ENDERECO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [ForeignKey("WMS_PEDIDO_CAPA")]
        public string REF_INTERNA { get; set; }

        public string FAT_CLIENTE { get; set; }
        public string FAT_ENDERECO { get; set; }
        public string FAT_COMPLEMENTO { get; set; }
        public string FAT_BAIRRO { get; set; }
        public string FAT_CIDADE { get; set; }
        public string FAT_UF { get; set; }
        public string FAT_CEP { get; set; }
        public string FAT_PAIS { get; set; }
        public string DEST_CLIENTE { get; set; }
        public string DEST_ENDERECO { get; set; }
        public string DEST_COMPLEMENTO { get; set; }
        public string DEST_BAIRRO { get; set; }
        public string DEST_CIDADE { get; set; }
        public string DEST_UF { get; set; }
        public string DEST_CEP { get; set; }
        public string DEST_PAIS { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual WMS_PEDIDO_CAPA WMS_PEDIDO_CAPA { get; set; }
    }
}
