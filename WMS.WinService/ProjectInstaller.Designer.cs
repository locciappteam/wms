﻿namespace WMS.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerWMS = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerWMS = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerWMS
            // 
            this.serviceProcessInstallerWMS.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerWMS.Password = null;
            this.serviceProcessInstallerWMS.Username = null;
            this.serviceProcessInstallerWMS.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessWMS_AfterInstall);
            // 
            // serviceInstallerWMS
            // 
            this.serviceInstallerWMS.Description = "Envia chamada WebAPI de Nota Fiscal faturada";
            this.serviceInstallerWMS.DisplayName = "LOcc - WMS";
            this.serviceInstallerWMS.ServiceName = "ServiceWMS";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerWMS,
            this.serviceInstallerWMS});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerWMS;
        private System.ServiceProcess.ServiceInstaller serviceInstallerWMS;
    }
}