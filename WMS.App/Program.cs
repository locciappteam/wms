﻿using System;
using System.Text;
using System.Threading.Tasks;
using WMS.Core.API;
using WMS.Core.Data;
using WMS.Core.Utilitarios;

namespace WMS.App
{
    public class Program
    {
        static void Main(string[] args) 
        {

            string User;
            string Pass; 
            Uri baseAddress;

            string ftpHost;
            string ftpPort;
            string ftpUser;
            string ftpPassword;
            string ftpPath;

            Console.WriteLine("");
            using (DataContext _ctx = new DataContext())
            {
                User = _ctx.WmsParametros.Find("USUARIO_API").VALOR;
                Pass = _ctx.WmsParametros.Find("SENHA_API").VALOR;
                baseAddress = new Uri(_ctx.WmsParametros.Find("BASE_ADDRESS").VALOR);

                ftpHost = _ctx.WmsParametros.Find("SENIOR_FTP_HOST_PROD").VALOR;
                ftpPort = _ctx.WmsParametros.Find("SENIOR_FTP_PORT_PROD").VALOR;
                ftpUser = _ctx.WmsParametros.Find("SENIOR_FTP_USER_PROD").VALOR;
                ftpPassword = _ctx.WmsParametros.Find("SENIOR_FTP_PASSWORD_PROD").VALOR;
                ftpPath = _ctx.WmsParametros.Find("SENIOR_FTP_FOLDER_INTEG_PROD").VALOR;
            }

            byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
            string Auth = Convert.ToBase64String(byteAuth);

           // CAMINHO PADRÃO 
           ImportaNotas.Cegid(DateTime.Today.AddDays(-5), new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1));
           ApiExportaNotasSenior.toFTP(ftpHost, ftpPort, ftpUser, ftpPassword, ftpPath);

        Task.Run(async () =>
        {
            // CAMINHO ALTERNATIVO (ENVIO PARA A EXVIEW)
            //await apiNotaFaturada.Post(Auth, baseAddress); // EXPORTA INFORMAÇÕES INTERNAS (CEGID)

            // CONTINUAÇÃO DO FLUXO ALTERNATIVO 
            //await apiDadosFaturamento.Get(Auth, baseAddress);
//await apiConfirmaDownload.Post(Auth, baseAddress);
        }).Wait();
        }

    }
}